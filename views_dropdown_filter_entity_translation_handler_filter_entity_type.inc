<?php

/**
 * @file
 * Definition of views_dropdown_filter_entity_translation_handler_filter_entity_type.
 */

/**
 * Extend the basic textfield filter handler with autocomplete.
 *
 * @ingroup views_filter_handlers
 */
class views_dropdown_filter_entity_translation_handler_filter_entity_type extends entity_translation_handler_filter_entity_type {
  // Exposed filter options.
  var $always_multiple = TRUE;

  function option_definition() {
    $options = parent::option_definition();

    $options['expose']['contains'] += array(
      'views_dropdown_filter' => array('default' => 0),
    );

    return $options;
  }

  function expose_form(&$form, &$form_state) {
    parent::expose_form($form, $form_state);

    // Build form elements for the right side of the exposed filter form.
    $form['expose'] += array(
      'views_dropdown_filter' => array(
        '#type' => 'checkbox',
        '#title' => t('Display as dropdown filter'),
        '#default_value' => $this->options['expose']['views_dropdown_filter'],
        '#description' => t('Use dropdown for this filter.'),
      ),
    );
  }

  public function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    if (empty($form_state['exposed']) || empty($this->options['expose']['views_dropdown_filter'])) {
      // It's not an exposed form or dropdown is not enabled.
      return;
    }

    if (empty($form['value']['#type'])) {
      // Not a textfield.
      return;
    }
  }

  /**
   * Validate that this filter instance has a corresponding autocomplete results field.
   *
   * @return array
   *   An array of errors triggered by this validation.
   */
  public function validate() {
    $errors = parent::validate();
    return $errors;
  }
}
