<?php
/**
 * Implements hook_views_data_alter().
 */
function views_dropdown_filter_views_data_alter(&$data) {
  foreach ($data as $data_type => $data_fields) {
    foreach ($data_fields as $fieldname => $data_field) {
      if (isset($data_field['filter']['handler'])) {
        // Extend the basic textfield filter handler with dropdown.
        if ($data_field['filter']['handler'] == 'views_handler_filter_string') {
          $data[$data_type][$fieldname]['filter']['handler'] = 'views_dropdown_filter_handler_filter_string';
        }
        elseif ($data_field['filter']['handler'] == 'views_handler_filter_numeric') {
          $data[$data_type][$fieldname]['filter']['handler'] = 'views_dropdown_filter_handler_filter_numeric';
	}
        elseif ($data_field['filter']['handler'] == 'entity_translation_handler_filter_language') {
          $data[$data_type][$fieldname]['filter']['handler'] = 'views_dropdown_filter_entity_translation_handler_filter_language';
	}
	elseif ($data_field['filter']['handler'] == 'entity_translation_handler_filter_entity_type') {
	  $data[$data_type][$fieldname]['filter']['handler'] = 'views_dropdown_filter_entity_translation_handler_filter_entity_type';
        }
      }
    }
  }
}
